MULTISITE_USER_PROFILE MODULE
-----------------------------

The multisite_user_profile module works in the following scenario:

You are sharing a user table among multiple Drupal sites.

You are NOT sharing profiles.

Each site enables profile module and creates a textfield.

As users fill out their profile, this module will mirror their profile data
in the data portion of the user object.

So each site has access to the other sites' user profile data by looking
in $user->multisite_user_profile.